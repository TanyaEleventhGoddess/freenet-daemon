# Conf file about wrapper for freenet-daemon init module

# Allow passing APP_NAME and APP_LONG_NAME
#APP_NAME_PASS_TO_WRAPPER=false

# Priority of the file
PRIORITY="10"

# Check if the pid inside PID file exists
PIDFILE_CHECK_PID=true

# Use a predefined command instead of the first parameter of the command line
#FIXED_COMMAND=console

# PASS_THROUGH tells the script tells to pass argument through the JVM as they are
# If FIXED_COMMAND is not specified they will al be passed, otherwise they will be passed starting with the second argument
#PASS_THROUGH=true

# If uncommented causes the wrapper to be shut down using an anchor file
#IGNORE_SIGNALS=true

# The wrapper will start the JVM asynchronously.
# Setting WAIT_AFTER_STARTUP to a positive number will cause the start command to delay for the indicated period of time (in seconds).
WAIT_AFTER_STARTUP=0

# If set, wait for the wrapper to report that the daemon has started
WAIT_FOR_STARTED_STATUS=true
WAIT_FOR_STARTED_TIMEOUT=120

# If set status, start_msg and stop_msg commands will print out detailed state information on the Wrapper and Java processes.
#DETAIL_STATUS=true

# If set, the 'pause' and 'resume' commands will be enabled.
# See the wrapper.pausable and wrapper.pausable.stop_jvm properties for more information.
#PAUSABLE=true

# Set path of SU
SU_BIN=su

# Set commands of SU
#SU_OPTS="-s /bin/bash"

# Path of the id command
ID_BIN=id

# Uncomment to show brief usage
#BRIEF_USAGE=true

# Use particular init system
USE_UPSTART=
USE_SYSTEMD=

# Flag for starting/stopping the wrapper without using the System Resource Controller
SKIP_SRC=

# Set runlevel
RUN_LEVEL=20

# Add files to source
FILES_TO_SOURCE=
